/*
  Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration
*/
#include "LArConditionsTest/LArConditionsTestAlg.h"
#include "LArConditionsTest/LArCondDataTest.h"
//#include "LArConditionsTest/FixLArIdMap.h"
#include "LArConditionsTest/LArFecLvTempDcsTest.h"
#include "LArConditionsTest/FixLArElecCalib.h"
#include "LArConditionsTest/TestLArConditionsTools.h" 
#include "LArConditionsTest/LArCablingTest.h"
#include "LArConditionsTest/LArIdMapConvert.h"
#include "LArConditionsTest/LArFebRodMapConvert.h"
#include "LArConditionsTest/LArShapeToSCShape.h"
#include "LArConditionsTest/LArOFCtoOFC.h"

DECLARE_COMPONENT( LArConditionsTestAlg )
DECLARE_COMPONENT( LArCondDataTest )
//DECLARE_COMPONENT( FixLArIdMap )
DECLARE_COMPONENT( LArFecLvTempDcsTest )
DECLARE_COMPONENT( FixLArElecCalib )
DECLARE_COMPONENT( TestLArConditionsTools ) 
DECLARE_COMPONENT( LArCablingTest )
DECLARE_COMPONENT( LArIdMapConvert )
DECLARE_COMPONENT( LArFebRodMapConvert )
DECLARE_COMPONENT( LArShapeToSCShape )
DECLARE_COMPONENT( LArOFCtoOFC )

