# $Id: CMakeLists.txt 804698 2017-05-11 16:03:27Z krumnack $
################################################################################
# Package: AsgTools
################################################################################


# Declare the package name:
atlas_subdir( AsgTools )

# Dependencies are taken based on what environment we are in:
if( XAOD_STANDALONE )
   set( deps Control/xAODRootAccessInterfaces Control/xAODRootAccess )
   set( libs xAODRootAccessInterfaces xAODRootAccess )
else()
   set( deps Control/xAODRootAccess Control/AthenaBaseComps Control/SGTools Database/IOVDbDataModel
      GaudiKernel )
   set( libs AthenaBaseComps SGTools IOVDbDataModel GaudiKernel )
endif()

# Declare the package's dependencies:
atlas_depends_on_subdirs( PUBLIC ${deps} )

# External dependencies:
find_package( Boost COMPONENTS regex )
find_package( ROOT COMPONENTS Core )

# Decide which sources to use:
if( XAOD_STANDALONE )
   atlas_add_root_dictionary( AsgTools AsgToolsCintDict
          ROOT_HEADERS  AsgTools/MsgLevel.h  AsgTools/MsgStream.h
          AsgTools/INamedInterface.h Root/LinkDef.h
          EXTERNAL_PACKAGES ROOT )
   set( sources Root/*.cxx  ${AsgToolsCintDict} )
else()
   set( sources Root/MsgLevel.cxx Root/AsgTool.cxx Root/AsgMetadataTool.cxx
      Root/AsgMessaging.cxx Root/ToolStore.cxx Root/AnaToolHandle.cxx
      Root/MessageCheck.cxx Root/UnitTest.cxx src/*.cxx )
endif()

# Component(s) in the package:
atlas_add_library( AsgTools
   AsgTools/*.h AsgTools/*.icc ${sources}
   PUBLIC_HEADERS AsgTools
   PRIVATE_INCLUDE_DIRS ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS}
   LINK_LIBRARIES ${libs}
   PRIVATE_LINK_LIBRARIES ${Boost_LIBRARIES} ${ROOT_LIBRARIES} )

# Select which xml files to use for dictionary generation

atlas_add_dictionary( AsgToolsDict
   AsgTools/AsgToolsDict.h AsgTools/selection.xml
   LINK_LIBRARIES AsgTools )

if( XAOD_STANDALONE )
    atlas_add_dictionary( AsgToolsStandAloneDict
    AsgTools/AsgToolsStandAloneDict.h AsgTools/selection_StandAlone.xml
    LINK_LIBRARIES AsgTools )
endif()

# Silence some warnings that are generated by the code on MacOS X:
set( extra_defs )
if( APPLE AND TARGET AsgTools )
   target_compile_options( AsgTools PUBLIC -Wno-tautological-undefined-compare )
endif()

# Test(s) in the package:

atlas_add_test( ut_asgtools_statuscode
   SOURCES test/ut_asgtools_statuscode.cxx
   LINK_LIBRARIES AsgTools )

atlas_add_test( ut_asgtools_toolstore
   SOURCES test/ut_asgtools_toolstore.cxx
   LINK_LIBRARIES AsgTools )

atlas_add_executable (asg_tools_test_init util/asg_tools_test_init.cxx
                      LINK_LIBRARIES AsgTools )

# This ought to work, but I can't figure it out.
#atlas_add_test (asg_tools_test_init_test COMMAND asg_tools_test_init asg::AsgTool)
#atlas_add_test (asg_tools_test_init_test SCRIPT asg_tools_test_init asg::AsgTool)
