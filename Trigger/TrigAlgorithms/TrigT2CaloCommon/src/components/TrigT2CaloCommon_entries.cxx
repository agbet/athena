//#include "TrigT2CaloCommon/T2CaloMon.h"
//#include "TrigT2CaloCommon/T2CaloL1Sim.h"
//#include "TrigT2CaloCommon/T2CaloMonTool.h"
#include "TrigT2CaloCommon/TrigDataAccess.h"
#include "TrigT2CaloCommon/TrigDataAccessATLFAST.h"
#include "TrigT2CaloCommon/T2CaloInitSvc.h"
#include "TrigT2CaloCommon/T2GeometryTool.h"
#include "../TrigCaloDataAccessSvc.h"
#include "../TestCaloDataAccess.h"

DECLARE_COMPONENT( T2CaloInitSvc )

//DECLARE_COMPONENT( T2CaloMon )
//DECLARE_COMPONENT( T2CaloL1Sim )
//DECLARE_COMPONENT( T2CaloMonTool )
DECLARE_COMPONENT( TrigDataAccess )
DECLARE_COMPONENT( TrigDataAccessATLFAST )
DECLARE_COMPONENT( T2GeometryTool )
DECLARE_COMPONENT( TrigCaloDataAccessSvc)
DECLARE_COMPONENT( TestCaloDataAccess )

